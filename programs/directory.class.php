<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

/**
 * audit_DirectorySet
 *
 * @property ORM_IntField           $id
 * @property ORM_StringField        $name
 * @property ORM_StringField        $description
 * @property ORM_IntField           $id_group
 * @property ORM_IntField           $id_dgowner
 * @property ORM_EnumField          $user_update
 * @property ORM_EnumField          $show_update_info
 * @property ORM_StringField        $ovml_list
 * @property ORM_StringField        $ovml_detail
 * @property ORM_EnumField          $disable_email
 */
class audit_DirectorySet extends ORM_RecordSet
{
    public function __construct()
    {
        parent::__construct();
        $this->setTableName('bab_db_directories');
        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('name'),
            ORM_TextField('description'),
            ORM_IntField('id_group'),
            ORM_IntField('id_dgowner'),
            ORM_EnumField('user_update', array('N' => 'N', 'Y' => 'Y')),
            ORM_EnumField('show_update_info', array('N' => 'N', 'Y' => 'Y')),
            ORM_StringField('ovml_list'),
            ORM_StringField('ovml_detail'),
            ORM_EnumField('disable_email', array('N' => 'N', 'Y' => 'Y'))
        );
    }
}

/**
 * audit_Directory
 *
 * @property int           $id
 * @property string        $name
 * @property string        $description
 * @property int           $id_group
 * @property int           $id_dgowner
 * @property string        $user_update
 * @property string        $show_update_info
 * @property string        $ovml_list
 * @property string        $ovml_detail
 * @property string        $disable_email
 */
class audit_Directory extends ORM_Record
{
    /**
     *
     * @return bool
     */
    public function isUserUpdatable()
    {
        return $this->user_update == 'Y';
    }

    /**
     *
     * @return bool
     */
    public function isUpdateInfoHidden()
    {
        return $this->show_update_info == 'N';
    }

    /**
     *
     * @return bool
     */
    public function isEmailDisabled()
    {
        return $this->disable_email == 'N';
    }

    /**
     * Check if the directory is visible by the specified user
     * @param int $user The id of the checked user. If null, check the current user
     * @return bool
     */
    public function isVisibleByUser($user = null)
    {
        if(!isset($user)){
            $user = bab_getUserId();
        }
        return bab_isAccessValid(BAB_DBDIRVIEW_GROUPS_TBL, $this->id, $user);
    }

    /**
     * Check if the directory can be modified by the specified user
     * @param int $user The id of the checked user. If null, check the current user
     * @return bool
     */
    public function isEditableByUser($user = null)
    {
        if(!isset($user)){
            $user = bab_getUserId();
        }
        return bab_isAccessValid(BAB_DBDIRUPDATE_GROUPS_TBL, $this->id, $user);
    }

    /**
     * Check if the specified user can add a contact in the directory
     * @param int $user The id of the checked user. If null, check the current user
     * @return bool
     */
    public function isAddableByUser($user = null)
    {
        if(!isset($user)){
            $user = bab_getUserId();
        }
        return bab_isAccessValid(BAB_DBDIRADD_GROUPS_TBL, $this->id, $user);
    }

    /**
     * Check if the specified user can remove a contact from the directory
     * @param int $user The id of the checked user. If null, check the current user
     * @return bool
     */
    public function isRemovableByUser($user = null)
    {
        if(!isset($user)){
            $user = bab_getUserId();
        }
        return bab_isAccessValid(BAB_DBDIRDEL_GROUPS_TBL, $this->id, $user);
    }

    /**
     * Check if the specified user can empty the directory
     * @param int $user The id of the checked user. If null, check the current user
     * @return bool
     */
    public function isEmptyableByUser($user = null)
    {
        if(!isset($user)){
            $user = bab_getUserId();
        }
        return bab_isAccessValid(BAB_DBDIREMPTY_GROUPS_TBL, $this->id, $user);
    }

    /**
     * Check if the specified user can import contacts into the directory
     * @param int $user The id of the checked user. If null, check the current user
     * @return bool
     */
    public function isImportableByUser($user = null)
    {
        if(!isset($user)){
            $user = bab_getUserId();
        }
        return bab_isAccessValid(BAB_DBDIRIMPORT_GROUPS_TBL, $this->id, $user);
    }

    /**
     * Check if the specified user can export the directory
     * @param int $user The id of the checked user. If null, check the current user
     * @return bool
     */
    public function isExportableByUser($user = null)
    {
        if(!isset($user)){
            $user = bab_getUserId();
        }
        return bab_isAccessValid(BAB_DBDIREXPORT_GROUPS_TBL, $this->id, $user);
    }

    /**
     * Returns all entries from the directory
     * @return audit_DirectoryEntry[]
     */
    public function getAllEntries()
    {
        $App = audit_App();
        $set = $App->DirectoryEntrySet();

        $contacts = $set->select($set->id_directory->is($this->id))->groupBy($set->id_user);
        return $contacts;
    }
}