<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once $GLOBALS['babInstallPath'] . 'utilit/filemanApi.php';
require_once $GLOBALS['babInstallPath'] . 'utilit/pathUtil.class.php';
require_once $GLOBALS['babInstallPath'] . 'utilit/uploadincl.php';

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/page.class.php';

$App = audit_App();
$App->includeController();


/**
 * This controller manages audit on file actions
 */
class audit_CtrlFile extends audit_Controller
{
    /**
     * @return string[]
     */
    private function getAccessRightNames()
    {
        $App = $this->App();

        $accessRightNames = array(
            BAB_FMUPLOAD_GROUPS_TBL => $App->translate('Upload'),
            BAB_FMDOWNLOAD_GROUPS_TBL => $App->translate('Download'),
            BAB_FMUPDATE_GROUPS_TBL => $App->translate('Update'),
            BAB_FMMANAGERS_GROUPS_TBL => $App->translate('Manage'),
            BAB_FMNOTIFY_GROUPS_TBL => $App->translate('Notified'),
            BAB_FMUNZIP_GROUPS_TBL => $App->translate('Unzip'),
            BAB_FMDOWNLOADHISTORY_GROUPS_TBL => $App->translate('Download history'),
        );

        return $accessRightNames;
    }


    /**
     *
     * @param array|null $groupIds
     * @param array|null $objectIds
     * @return array[]|null
     */
    public function computeRights($accessRights = null, $groupIds = null, $objectIds = null)
    {
        $App = $this->App();

        $folderSet = $App->FolderSet();

        $accessRightNames = $this->getAccessRightNames();

        if ((!isset($groupIds) || empty ($groupIds)) && (!isset($objectIds) || empty($objectIds))) {
            return null;
        }


        $accessRights = array_keys($accessRightNames);


        if (!isset($groupIds) || empty ($groupIds)) {
            $grs = bab_getGroups();
            $groups = array();
            foreach ($grs['id'] as $i => $grId) {
                $groups[$grId] = $grs['name'][$i];
            }
        } else {
            foreach ($groupIds as $grId) {
                if ($grId === '') {
                    continue;
                }
                $groups[$grId] = bab_getGroupName($grId);
            }
        }

        if (isset($objectIds) && !empty($objectIds)) {
            $objectGroupIds = array();
            foreach ($objectIds as $objectId) {
                foreach ($accessRights as $accessRight) {
                    $grIds = bab_getGroupsAccess($accessRight, $objectId);
                    foreach ($grIds as $grId) {
                        $objectGroupIds[$grId] = $groups[$grId];
                    }
                }
            }
        } else {
            $objectGroupIds = $groups;
        }

        $objectNames = array();


        require_once $GLOBALS['babInstallPath'] . 'utilit/userincl.php';
        require_once $GLOBALS['babInstallPath'] . 'utilit/delincl.php';
        require_once $GLOBALS['babInstallPath'] . 'utilit/loginIncl.php';

        $user = bab_getUserByNickname('__audit__');
        if (!isset($user)) {
            $userId = bab_registerUser('Audit', 'AUDIT', '', 'audit@example.com', '__audit__', 'audit123', 'audit123', 1, $error, false);
        } else {
            $userId = $user['id'];
        }

        $audit = array();

        $audit['rightsGRO'] = array();
        $audit['rightsGOR'] = array();
        $audit['rightsORG'] = array();
        $audit['rightsOGR'] = array();
        $audit['rightsROG'] = array();
        $audit['rightsRGO'] = array();


        foreach ($accessRights as $accessRightTable) {
            $R = $App->translate('Access') . ': ' . $accessRightNames[$accessRightTable];
            foreach ($objectGroupIds as $groupId => $groupName) {
                $G = $App->translate('Group') . ': ' . $groupName;
                bab_addUserToGroup($userId, $groupId);
                $objects = bab_getAccessibleObjects($accessRightTable, $userId);
                foreach ($objects as $objectId) {
                    if (isset($objectIds) && !empty($objectIds) && !in_array($objectId, $objectIds)) {
                        continue;
                    }
                    if (!isset($objectNames[$objectId])) {
                        $folder = $folderSet->get($objectId);
                        $objectNames[$objectId] = $folder->sRelativePath . $folder->folder;
                    }
                    $O = $App->translate('Folder') . ': ' . $objectNames[$objectId];
                    $audit['rightsGRO'][$G][$R][$O] = $objectId;
                    $audit['rightsGOR'][$G][$O][$R] = $objectId;
                    $audit['rightsORG'][$O][$R][$G] = $objectId;
                    $audit['rightsOGR'][$O][$G][$R] = $objectId;
                    $audit['rightsROG'][$R][$O][$G] = $objectId;
                    $audit['rightsRGO'][$R][$G][$O] = $objectId;
                }

                bab_removeUserFromGroup($userId, $groupId);
            }
        }

        bab_deleteUser($userId);

        return $audit;
    }


    /**
     * @return boolean
     */
    public function invalidateCache()
    {
        $App = $this->App();

        $_SESSION['audit']['cache'] = null;
        $this->addReloadSelector('.depends-cached-rights');
        $this->addMessage($App->translate('Updating...'));

        return true;
    }

    /**
     *
     * @return boolean
     */
    public function updateRights()
    {
        $App = $this->App();

        $folderSet = $App->FolderSet();

        $accessRightNames = $this->getAccessRightNames();

        $accessRights = $_SESSION['audit/filters/folderAccessRights'];
        $groupIds = $_SESSION['audit/filters/groupIds'];
        $objectIds = $_SESSION['audit/filters/folderIds'];

        $this->addReloadSelector('.depends-cached-rights');

        if ((!isset($groupIds) || empty ($groupIds)) && (!isset($objectIds) || empty($objectIds))) {
            $_SESSION['audit']['cache'] = null;
            return true;
        }


        if (!isset($accessRights)) {
            $accessRights = array_keys($accessRightNames);
        }

        if (!isset($groupIds) || empty ($groupIds)) {
            $grs = bab_getGroups();
            $groups = array();
            foreach ($grs['id'] as $i => $grId) {
                $groups[$grId] = $grs['name'][$i];
            }
        } else {
            foreach ($groupIds as $grId) {
                if ($grId === '') {
                    continue;
                }
                $groups[$grId] = bab_getGroupName($grId);
            }
        }

        if (isset($objectIds) && !empty($objectIds)) {
            $objectGroupIds = array();
            foreach ($objectIds as $objectId) {
                foreach ($accessRights as $accessRight) {
                    $grIds = bab_getGroupsAccess($accessRight, $objectId);
                    foreach ($grIds as $grId) {
                        $objectGroupIds[$grId] = $groups[$grId];
                    }
                }
            }
        } else {
            $objectGroupIds = $groups;
        }

        $objectNames = array();


        require_once $GLOBALS['babInstallPath'] . 'utilit/userincl.php';
        require_once $GLOBALS['babInstallPath'] . 'utilit/delincl.php';
        require_once $GLOBALS['babInstallPath'] . 'utilit/loginIncl.php';

        $user = bab_getUserByNickname('__audit__');
        if (!isset($user)) {
            $userId = bab_registerUser('Audit', 'AUDIT', '', 'audit@example.com', '__audit__', 'audit123', 'audit123', 1, $error, false);
        } else {
            $userId = $user['id'];
        }

        $_SESSION['audit']['cache'] = array();
        $_SESSION['audit']['cache']['rightsGRO'] = array();
        $_SESSION['audit']['cache']['rightsGOR'] = array();
        $_SESSION['audit']['cache']['rightsORG'] = array();
        $_SESSION['audit']['cache']['rightsOGR'] = array();
        $_SESSION['audit']['cache']['rightsROG'] = array();
        $_SESSION['audit']['cache']['rightsRGO'] = array();


        foreach ($accessRights as $accessRightTable) {
            $R = $App->translate('Access') . ': ' . $accessRightNames[$accessRightTable];
            foreach ($objectGroupIds as $groupId => $groupName) {
                $G = $App->translate('Group') . ': ' . $groupName;
                bab_addUserToGroup($userId, $groupId);
                $objects = bab_getAccessibleObjects($accessRightTable, $userId);
                foreach ($objects as $objectId) {
                    if (isset($objectIds) && !empty($objectIds) && !in_array($objectId, $objectIds)) {
                        continue;
                    }
                    if (!isset($objectNames[$objectId])) {
                        $folder = $folderSet->get($objectId);
                        $objectNames[$objectId] = $folder->sRelativePath . $folder->folder;
                    }
                    $O = $App->translate('Folder') . ': ' . $objectNames[$objectId];
                    $_SESSION['audit']['cache']['rightsGRO'][$G][$R][$O] = $objectId;
                    $_SESSION['audit']['cache']['rightsGOR'][$G][$O][$R] = $objectId;
                    $_SESSION['audit']['cache']['rightsORG'][$O][$R][$G] = $objectId;
                    $_SESSION['audit']['cache']['rightsOGR'][$O][$G][$R] = $objectId;
                    $_SESSION['audit']['cache']['rightsROG'][$R][$O][$G] = $objectId;
                    $_SESSION['audit']['cache']['rightsRGO'][$R][$G][$O] = $objectId;
                }

                bab_removeUserFromGroup($userId, $groupId);
            }
        }

        bab_deleteUser($userId);

        return true;
    }


    /**
     *
     * @param string $itemId
     * @return Widget_VBoxLayout
     */
    public function displayRights($itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $box = $W->VBoxItems()->setVerticalSpacing(2, 'em');
        if (isset($itemId)) {
            $box->setId($itemId);
        }
        $box->setReloadAction($this->proxy()->displayRights($box->getId()));

        $groupings = $_SESSION['audit/grouping'];
        if (!isset($groupings) || empty($groupings)) {
            $groupings = array('objects' => 'objects', 'groups' => 'groups', 'rights' => 'rights');
        }

        $gro = implode(',', $groupings);

        if (!isset($_SESSION['audit']['cache'])) {
            $this->updateRights();
        }

        switch ($gro) {
            case 'objects,groups,rights':
                $level1 = $_SESSION['audit']['cache']['rightsOGR'];
                break;
            case 'objects,rights,groups':
                $level1 = $_SESSION['audit']['cache']['rightsORG'];
                break;
            case 'rights,groups,objects':
                $level1 = $_SESSION['audit']['cache']['rightsRGO'];
                break;
            case 'rights,objects,groups':
                $level1 = $_SESSION['audit']['cache']['rightsROG'];
                break;
            case 'groups,objects,rights':
                $level1 = $_SESSION['audit']['cache']['rightsGOR'];
                break;
            case 'groups,rights,objects':
                $level1 = $_SESSION['audit']['cache']['rightsGRO'];
                break;
        }

        if (!isset($level1)) {
            $box->addItem(
                $W->Label($App->translate('Select a group or folder first'))
                ->setSizePolicy('alert alert-info')
            );
            $box->addClass('depends-cached-rights');
            return $box;
        }

        bab_Sort::ksort($level1);


        foreach ($level1 as $level1Label => $level2) {
            $level1Section = $W->Section(
                $level1Label,
                $W->VBoxItems(),
                4
            )->setFoldable(true);
            bab_Sort::ksort($level2);
            foreach ($level2 as $level2Label => $level3) {
                $level2Section = $W->Section(
                    $level2Label,
                    $W->ListItems(),
                    5
                )->setFoldable(true);
                bab_Sort::ksort($level3);
                foreach ($level3 as $level3Label => $objectId) {

                    if (array_keys($groupings)[2] == 'objects') {
                        $item = $W->Link(
                            $level3Label,
                            $App->Controller()->File()->displayRightsNoCache('', array($objectId))
                        )->setOpenMode(Widget_Link::OPEN_DIALOG);
                    } else {
                        $item = $W->Label($level3Label);
                    }
                    $level2Section->addItem($item);
                }
                $level1Section->addItem($level2Section);
            }
            $box->addItem($level1Section);
        }

        $box->addClass('depends-cached-rights', 'depends-grouping');

        return $box;
    }



    /**
     *
     * @param string $itemId
     * @return Widget_VBoxLayout
     */
    public function displayRightsNoCache($groupIds = null, $objectIds = null, $groupings = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $box = $W->VBoxItems()->setVerticalSpacing(2, 'em');

        if (!isset($groupings) || empty($groupings)) {
            $groupings = array('objects', 'groups', 'rights');
        }

        if (!isset($groupIds)) {
            $groupIds = $_SESSION['audit/filters/groupIds'];
        }

        if (!isset($objectIds)) {
            $objectIds = $_SESSION['audit/filters/folderIds'];
        }

        $audit = $this->computeRights($groupIds, $objectIds);

        $gro = implode(',', $groupings);

        switch ($gro) {
            case 'objects,groups,rights':
                $level1 = $audit['rightsOGR'];
                break;
            case 'objects,rights,groups':
                $level1 = $audit['rightsORG'];
                break;
            case 'rights,groups,objects':
                $level1 = $audit['rightsRGO'];
                break;
            case 'rights,objects,groups':
                $level1 = $audit['rightsROG'];
                break;
            case 'groups,objects,rights':
                $level1 = $audit['rightsGOR'];
                break;
            case 'groups,rights,objects':
                $level1 = $audit['rightsGRO'];
                break;
        }

        if (!isset($level1)) {
            $box->addItem(
                $W->Label($App->translate('Select a group or folder first'))
                ->setSizePolicy('alert alert-info')
            );
            return $box;
        }

        bab_Sort::ksort($level1);
        foreach ($level1 as $level1Label => $level2) {
            $level1Section = $W->Section(
                $level1Label,
                $W->VBoxItems(),
                4
            )->setFoldable(true);
            bab_Sort::ksort($level2);
            foreach ($level2 as $level2Label => $level3) {
                $level2Section = $W->Section(
                    $level2Label,
                    $W->ListItems(),
                    5
                )->setFoldable(true);
                bab_Sort::ksort($level3);
                foreach ($level3 as $level3Label => $level4) {
                    $level2Section->addItem($W->Label($level3Label));
                }
                $level1Section->addItem($level2Section);
            }
            $box->addItem($level1Section);
        }

        return $box;
    }



    /**
     *
     * @param array|null $accessRights
     * @param array|null $groupIds
     * @param array|null $userId
     * @param array|null $objectIds
     * @return boolean
     */
    public function saveFilters($accessRights = null, $groupIds = null, $userId = null, $objectIds = null)
    {
        $App = $this->App();
        if (isset($accessRights)) {
            $_SESSION['audit/filters/folderAccessRights'] = $accessRights;
            $values = array();
            foreach ($accessRights as $accessRight) {
                if ($accessRight !== '') {
                    $values[] = $accessRight;
                }
            }
            $_SESSION['audit/filters/folderAccessRights'] = $values;
        }

        if (isset($groupIds)) {
            $values = array();
            foreach ($groupIds as $groupId) {
                if ($groupId !== '') {
                    $values[] = $groupId;
                }
            }
            $_SESSION['audit/filters/groupIds'] = $values;
        }

        if (isset($objectIds)) {
            $values = array();
            foreach ($objectIds as $objectId) {
                if ($objectId !== '') {
                    $values[] = $objectId;
                }
            }
            $_SESSION['audit/filters/folderIds'] = $values;
        }

        $this->invalidateCache();

        $this->addReloadSelector('.depends-filters');

        return true;
    }




    /**
     *
     * @param unknown $grouping
     * @return boolean
     */
    public function saveGrouping($grouping = null)
    {
        if (isset($grouping)) {
            $_SESSION['audit/grouping'] = $grouping;
        }

        $this->addReloadSelector('.depends-grouping');

        return true;
    }


    /**
     *
     * @return Widget_BabPage
     */
    public function clearFolders()
    {
        $_SESSION['audit/filters/folderIds'] = null;

        $this->invalidateCache();

        $this->addReloadSelector('.depends-filters');

        return true;
    }

    /**
     *
     * @return Widget_BabPage
     */
    public function selectFolders()
    {
        $W = bab_Widgets();
        $App = $this->App();

        $page = $W->BabPage();

        $page->setTitle('Select folders');

        $folderSet = $App->FolderSet();

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');

        $form = $W->Form(null, $box);

        $form->setHiddenValue('tg', $this->getControllerTg());

        $foldersBox = $W->VBoxItems();
        $foldersBox->addClass('widget-80vh');
        $box->addItem($foldersBox);

        $delegationNames = array('0' => 'Global');
        $delegationColors = array('0' => 'CCCCCC');
        $delegations = bab_getDelegations();

        foreach ($delegations as $delegation) {
            $delegationNames[$delegation['id']] = $delegation['name'];
            $delegationColors[$delegation['id']] = $delegation['color'];
        }

        $folders = $folderSet->select();
        $folders->orderAsc($folderSet->id_dgowner);
        $folders->orderAsc($folderSet->sRelativePath->concat($folderSet->folder));

        foreach ($folders as $folder) {
            $foldersBox->addItem(
                $W->HBoxItems(
                    $W->CheckBox()
                        ->setName(array('objectIds', $folder->id))
                        ->setUncheckedValue('')
                        ->setCheckedValue($folder->id)
                        ->setValue(in_array($folder->id, $_SESSION['audit/filters/folderIds']) ? $folder->id : ''),
                    $W->FlowItems(
                        $W->Label($delegationNames[$folder->id_dgowner])
                            ->setCanvasOptions(Widget_Item::options()->backgroundColor('#' . $delegationColors[$folder->id_dgowner]))
                            ->addClass('widget-strong', 'widget-small', 'badge'),
                        $W->Label($folder->sRelativePath . $folder->folder)->addClass('widget-small')
                    )->setHorizontalSpacing(1, 'em')
                )->setHorizontalSpacing(0.5, 'em')
                ->setVerticalAlign('middle')
            );
        }

        $submitButton = $W->SubmitButton();
        $submitButton->addClass('widget-close-dialog');
        $submitButton->setAjaxAction($this->proxy()->saveFilters());

        $form->addItem($submitButton);

        $page->addItem($form);

        return $page;
    }



    /**
     *
     * @return Widget_BabPage
     */
    public function clearGroups()
    {
        $_SESSION['audit/filters/groupIds'] = null;

        $this->invalidateCache();

        $this->addReloadSelector('.depends-filters');

        return true;
    }


    /**
     *
     * @return Widget_BabPage
     */
    public function selectGroups()
    {
        $W = bab_Widgets();
        $App = $this->App();

        $page = $W->BabPage();

        $page->setTitle('Select groups');

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');

        $form = $W->Form(null, $box);

        $form->setHiddenValue('tg', $this->getControllerTg());

        $groupsBox = $W->VBoxItems();
        $groupsBox->addClass('widget-80vh');
        $box->addItem($groupsBox);

        $grs = bab_getGroups();
        foreach ($grs['id'] as $i => $grId) {
            $groupsBox->addItem(
                $W->HBoxItems(
                    $W->CheckBox()
                        ->setName(array('groupIds', $grId))
                        ->setUncheckedValue('')
                        ->setCheckedValue($grId)
                        ->setValue(in_array($grId, $_SESSION['audit/filters/groupIds']) ? $grId : ''),
                    $W->Label($grs['name'][$i])->addClass('widget-nowrap')
                )->setHorizontalSpacing(0.5, 'em')
                ->setVerticalAlign('middle')
            );
        }

        $submitButton = $W->SubmitButton();
        $submitButton->addClass('widget-close-dialog');
        $submitButton->setAction($this->proxy()->saveFilters());
        $submitButton->setAjaxAction();

        $form->addItem($submitButton);

        $page->addItem($form);
        return $page;
    }


    /**
     *
     * @return Widget_BabPage
     */
    public function clearAccessRights()
    {
        $_SESSION['audit/filters/folderAccessRights'] = null;

        $this->invalidateCache();

        $this->addReloadSelector('.depends-filters');

        return true;
    }



    /**
     *
     * @return Widget_BabPage
     */
    public function selectAccessRights()
    {
        $W = bab_Widgets();
        $App = $this->App();

        $page = $W->BabPage();

        $page->setTitle('Select access rights');

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');

        $form = $W->Form(null, $box);

        $form->setHiddenValue('tg', $this->getControllerTg());

        $accessRightsBox = $W->VBoxItems();
        $accessRightsBox->addClass('widget-80vh');
        $box->addItem($accessRightsBox);

        $accessRightNames = $this->getAccessRightNames();


        foreach ($accessRightNames as $accessRight => $label) {
            $accessRightsBox->addItem(
                $W->HBoxItems(
                    $W->CheckBox()
                    ->setName(array('accessRights', $accessRight))
                    ->setUncheckedValue('')
                    ->setCheckedValue($accessRight)
                    ->setValue(in_array($accessRight, $_SESSION['audit/filters/folderAccessRights']) ? $accessRight : ''),
                    $W->Label($label)->addClass('widget-nowrap')
                )->setHorizontalSpacing(0.5, 'em')
                ->setVerticalAlign('middle')
            );
        }

        $submitButton = $W->SubmitButton();
        $submitButton->addClass('widget-close-dialog');
        $submitButton->setAction($this->proxy()->saveFilters());
        $submitButton->setAjaxAction();

        $form->addItem($submitButton);

        $page->addItem($form);
        return $page;
    }



    /**
     *
     * @return Widget_BabPage
     */
    public function selectGrouping()
    {
        $W = bab_Widgets();
        $App = $this->App();

        $page = $W->BabPage();

        $page->setTitle('Select grouping');

        $box = $W->VBoxItems();
        $box->setVerticalSpacing(1, 'em');

        $form = $W->Form(null, $box);
        $form->setReadOnly(true);
        $form->setHiddenValue('tg', $this->getControllerTg());

        $sortableBox = $W->VBoxItems();
        $sortableBox->sortable();
        $sortableBox->addClass(Func_Icons::ICON_LEFT_16);

        $box->addItem($sortableBox);

        $groupingNames = array(
            'objects' => $App->translate('Folders'),
            'groups' => $App->translate('Groups'),
            'rights' => $App->translate('Rights')
        );

        $groupingClasses = array(
            'objects' => Func_Icons::APPS_FILE_MANAGER,
            'groups' => Func_Icons::OBJECTS_GROUP,
            'rights' => Func_Icons::ACTIONS_SET_ACCESS_RIGHTS
        );

        $groupings = $_SESSION['audit/grouping'];
        if (!isset($groupings) || empty($groupings)) {
            $groupings = array('objects', 'groups', 'rights');
        }

        foreach ($groupings as $grouping) {
            $sortableBox->addItem(
                $W->FlowItems(
                    $W->Hidden()
                        ->setValue($grouping)
                        ->setName(array('grouping', $grouping)),
                    $W->Label($groupingNames[$grouping])
                        ->addClass('icon', 'badge', $groupingClasses[$grouping])
                )->setSizePolicy('widget-10em')
            );
        }

        $submitButton = $W->SubmitButton()->setLabel($App->translate('Save grouping'));
        $submitButton->addClass('widget-close-dialog');
        $submitButton->setAction($this->proxy()->saveGrouping());
        $submitButton->setAjaxAction();

        $form->addItem($submitButton);

        $page->addItem($form);
        return $page;
    }



    /**
     *
     * @return Widget_BabPage
     */
    public function editFilters()
    {
        $W = bab_Widgets();
        $App = $this->App();

        $page = $W->BabPage(null, $W->VBoxItems()->setVerticalSpacing(1, 'em'));

        $page->setTitle($App->translate('Filemanager audit'));

        $page->addItem(
            $W->FlowItems(
                $W->Section(
                    $App->translate('Groups'),
                    $this->groupsFilter()
                )->addClass('compact'),
                $W->Section(
                    $App->translate('Documents'),
                    $this->objectsFilter()
                )->addClass('compact'),
                $W->Section(
                    $App->translate('Access rights'),
                    $this->accessRightsFilter()
                )->addClass('compact'),
                $W->Section(
                    $App->translate('Grouping'),
                    $this->groupingFilters()
                )->addClass('compact')
            )->setHorizontalSpacing(5, 'em')
            ->setVerticalAlign('top')
        );


        $page->addItem($this->displayRights());

        return $page;
    }



    /**
     *
     * @param unknown $itemId
     * @return Widget_FlowLayout
     */
    public function groupsFilter($itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();

        $box = $W->VBoxItems();

        if (isset($itemId)) {
            $box->setId($itemId);
        }

        $box->addItem(
            $W->FlowItems(
                $W->Link(
                    $App->translate('Select groups'),
                    $this->proxy()->selectGroups()
                )->setOpenMode(Widget_Link::OPEN_DIALOG)
                ->addClass('widget-actionbutton', 'icon', Func_Icons::OBJECTS_GROUP),
                $W->Link(
                    '',
                    $this->proxy()->clearGroups()
                )->setTitle($App->translate('Clear selected groups'))
                ->setAjaxAction(null, '')
                ->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_EDIT_DELETE)
            )->addClass(Func_Icons::ICON_LEFT_16)
        );

        $groupIds = $_SESSION['audit/filters/groupIds'];

        if (isset($groupIds) && !empty($groupIds)) {
            $items = $W->VBoxItems();
            foreach ($groupIds as $groupId) {
                $name = bab_getGroupName($groupId);
                $items->addItem($W->Label($name));

            }
            $box->addItem($items);
        } else {
            $box->addItem(
                $W->Label($App->translate('-- All groups --'))
                    ->addClass('widget-long-description')
            );
        }

        $box->addClass('depends-filters');
        $box->setReloadAction($this->proxy()->groupsFilter($box->getId()));

        return $box;
    }


    /**
     *
     * @param unknown $itemId
     * @return Widget_FlowLayout
     */
    public function objectsFilter($itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();

        $box = $W->VBoxItems();

        if (isset($itemId)) {
            $box->setId($itemId);
        }

        $box->addItem(
            $W->FlowItems(
                $W->Link(
                    $App->translate('Select folders'),
                    $this->proxy()->selectFolders()
                )->setOpenMode(Widget_Link::OPEN_DIALOG)
                ->addClass('widget-actionbutton', 'icon', Func_Icons::APPS_FILE_MANAGER),
                $W->Link(
                    '',
                    $this->proxy()->clearFolders()
                )->setTitle($App->translate('Clear selected folders'))
                ->setAjaxAction(null, '')
                ->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_EDIT_DELETE)
            )->addClass(Func_Icons::ICON_LEFT_16)
        );

        $objectIds = $_SESSION['audit/filters/folderIds'];

        if (isset($objectIds) && !empty($objectIds)) {
            $folderSet = $App->FolderSet();

            $items = $W->VBoxItems();
            foreach ($objectIds as $objectId) {
                $folder = $folderSet->get($objectId);
                $items->addItem($W->Label($folder->sRelativePath . $folder->folder));

            }
            $box->addItem($items);
        } else {
            $box->addItem(
                $W->Label($App->translate('-- All folders --'))
                    ->addClass('widget-long-description')
            );
        }

        $box->addClass('depends-filters');
        $box->setReloadAction($this->proxy()->objectsFilter($box->getId()));

        return $box;
    }


    /**
     *
     * @param unknown $itemId
     * @return Widget_FlowLayout
     */
    public function accessRightsFilter($itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();

        $box = $W->VBoxItems();

        if (isset($itemId)) {
            $box->setId($itemId);
        }

        $box->addItem(
            $W->FlowItems(
                $W->Link(
                    $App->translate('Select access rights'),
                    $this->proxy()->selectAccessRights()
                )->setOpenMode(Widget_Link::OPEN_DIALOG)
                ->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_SET_ACCESS_RIGHTS),
                $W->Link(
                    '',
                    $this->proxy()->clearAccessRights()
                )->setTitle($App->translate('Clear selected access rights'))
                ->setAjaxAction(null, '')
                ->addClass('widget-actionbutton', 'icon', Func_Icons::ACTIONS_EDIT_DELETE)
            )->addClass(Func_Icons::ICON_LEFT_16)
        );

        $accessRightNames = $this->getAccessRightNames();

        $accessRights = $_SESSION['audit/filters/folderAccessRights'];

        if (isset($accessRights) && !empty($accessRights)) {
            foreach ($accessRights as $accessRight) {
                $box->addItem($W->Label($accessRightNames[$accessRight]));
            }
        } else {
            $box->addItem(
                $W->Label($App->translate('-- All access rights --'))
                    ->addClass('widget-long-description')
            );
        }

        $box->addClass('depends-filters');
        $box->setReloadAction($this->proxy()->accessRightsFilter($box->getId()));

        return $box;
    }



    public function groupingFilters($itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();

        $box = $W->VBoxItems();

        if (isset($itemId)) {
            $box->setId($itemId);
        }

        $box->addItem(
            $W->Link(
                $App->translate('Select grouping order'),
                $this->proxy()->selectGrouping()
            )->setOpenMode(Widget_Link::OPEN_DIALOG)
            ->addClass('widget-actionbutton')
        );


        $groupingNames = array(
            'objects' => $App->translate('Folders'),
            'groups' => $App->translate('Groups'),
            'rights' => $App->translate('Rights')
        );

        $groupingClasses = array(
            'objects' => Func_Icons::APPS_FILE_MANAGER,
            'groups' => Func_Icons::OBJECTS_GROUP,
            'rights' => Func_Icons::ACTIONS_SET_ACCESS_RIGHTS
        );

        $groupings = $_SESSION['audit/grouping'];
        if (!isset($groupings) || empty($groupings)) {
            $groupings = array('objects', 'groups', 'rights');
        }

        $groupingBox = $W->VBoxItems();
        $box->addItem($groupingBox);
        $groupingBox->addClass(Func_Icons::ICON_LEFT_16);

        foreach ($groupings as $grouping) {
            $groupingBox->addItem(
                $W->FlowItems(
                    $W->Label($groupingNames[$grouping])
                        ->addClass('icon', $groupingClasses[$grouping])
                )
            );
        }

        $box->addClass('depends-grouping');
        $box->setReloadAction($this->proxy()->groupingFilters($box->getId()));

        return $box;
    }
}
