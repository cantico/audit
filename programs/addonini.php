;<?php/*

[general]
name="audit"
version="0.0.1"
addon_type="EXTENSION"
encoding="UTF-8"
mysql_character_set_database="latin1,utf8"
description="Audit"
description.fr="Audit"
delete=1
ov_version="8.6.97"
php_version="5.4.0"
addon_access_control="1"
author="Laurent Choulette (laurent.choulette@cantico.fr)"
icon="icon48.png"
image="thumbnail.png"
license="GPL-2.0+"
tags="extension,portlet"

[addons]
libapp              ="0.0.1"
widgets             ="1.0.91"
jquery              ="1.11.1.3"
LibTranslate        =">=1.12.0rc3.01"
LibOrm				="0.11.9"



[functionalities]
jquery="Available"

; */?>