<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/functions.php';


function audit_onDeleteAddon()
{
    return true;
}



function audit_upgrade($version_base, $version_ini)
{
    global $babDB;

    $babDB->db_query('SET FOREIGN_KEY_CHECKS = 0');


    require_once $GLOBALS['babInstallPath'] . 'utilit/upgradeincl.php';
    require_once $GLOBALS['babInstallPath'] . 'utilit/functionalityincl.php';
    require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';

    require_once dirname(__FILE__) . '/audit.php';

    $functionalities = new bab_functionalities();


    $addon = bab_getAddonInfosInstance('audit');
    $addonPhpPath = $addon->getPhpPath();

    if ($functionalities->registerClass('Func_App_Audit', $addonPhpPath . 'audit.php')) {
        echo(bab_toHtml('Functionality "Func_App_Audit" registered.'));
    }


    $App = audit_App();

//     $mysqlbackend = new ORM_MySqlBackend($GLOBALS['babDB']);
//     $customFieldSet = $App->CustomFieldSet();
//     $sql = $mysqlbackend->setToSql($customFieldSet) . "\n";

//     $fileSet = $App->FileSet();

//     $synchronize = new bab_synchronizeSql();
//     $synchronize->fromSqlString($sql);

//     $synchronize = $App->synchronizeSql('audit_');
//     $synchronize->addOrmSet($fileSet);


    $addon->addEventListener('bab_eventBeforeSiteMapCreated', 'audit_onSiteMapItems', 'init.php');

    return true;
}



/**
 * Sitemap creation
 * @param bab_eventBeforeSiteMapCreated $event
 * @return mixed
 */
function audit_onSiteMapItems(bab_eventBeforeSiteMapCreated $event)
{
    require_once dirname(__FILE__).'/functions.php';

    bab_functionality::includefile('Icons');

    $App = audit_App();


    $item = $event->createItem('audit_root');
    $item->setLabel($App->translate('Audit'));
    $item->setLink($App->Controller()->Audit()->menu()->url());
    $item->setPosition(array('root', 'DGAll', 'babUser', 'babUserSectionAddons'));
    $item->addIconClassname('apps-addon-filemanager');
    $event->addFolder($item);
}


