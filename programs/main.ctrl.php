<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */


/* @var $App Func_App_Audit */
$App = bab_functionality::get('App/Audit');


$App->includeController();


/**
 * @method Func_App_Audit App()
 */
class audit_Controller extends app_Controller
{
    /**
     * {@inheritDoc}
     * @see app_Controller::getControllerTg()
     */
    protected function getControllerTg()
    {
        return 'addon/audit/main';
    }


    /**
     * Get object name to use in URL from the controller classname
     * @param string $classname
     * @return string
     */
    protected function getObjectName($classname)
    {
        $prefix = strlen('audit_Ctrl');
        return strtolower(substr($classname, $prefix));
    }


    /**
     * @return audit_CtrlAudit
     */
    public function Audit($proxy = true)
    {
        require_once dirname(__FILE__) . '/audit.ctrl.php';
        return $this->App()->ControllerProxy('audit_CtrlAudit', $proxy);
    }

    /**
     * @return audit_CtrlFile
     */
    public function File($proxy = true)
    {
        require_once dirname(__FILE__) . '/file.ctrl.php';
        return $this->App()->ControllerProxy('audit_CtrlFile', $proxy);
    }


    /**
     * @return audit_CtrlDirectory
     */
    public function Directory($proxy = true)
    {
        require_once dirname(__FILE__) . '/directory.ctrl.php';
        return $this->App()->ControllerProxy('audit_CtrlDirectory', $proxy);
    }
}
