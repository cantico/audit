<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once $GLOBALS['babInstallPath'] . 'utilit/filemanApi.php';
require_once $GLOBALS['babInstallPath'] . 'utilit/pathUtil.class.php';
require_once $GLOBALS['babInstallPath'] . 'utilit/uploadincl.php';

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/page.class.php';

$App = audit_App();
$App->includeController();


/**
 * This controller manages audit actions
 */
class audit_CtrlAudit extends audit_Controller
{

    /**
     *
     * @return Widget_BabPage
     */
    public function menu()
    {
        $W = bab_Widgets();
        $App = $this->App();

        $page = $W->BabPage();

        $page->setTitle('Menu');

        $box = $W->FlowItems();
        $page->addItem($box);

        $box->addClass(Func_Icons::ICON_LEFT_32);
        $box->addItem(
            $W->Link(
                $App->translate('Groups'),
                $App->Controller()->Audit()->groups()
            )->addClass('icon', Func_Icons::OBJECTS_GROUP)
            ->setSizePolicy('widget-15em')
        );
        $box->addItem(
            $W->Link(
                $App->translate('Documents'),
                $App->Controller()->File()->editFilters()
            )->addClass('icon', Func_Icons::APPS_FILE_MANAGER)
            ->setSizePolicy('widget-15em')
        );

        $box->addItem(
            $W->Link(
                $App->translate('Directories'),
                $App->Controller()->Directory()->editFilters()
            )->addClass('icon', Func_Icons::APPS_DIRECTORIES)
            ->setSizePolicy('widget-15em')
        );

        return $page;
    }


    /**
     *
     */
    public function groups()
    {
        $W = bab_Widgets();
        $App = $this->App();

        $page = $W->BabPage();

        $page->setTitle('Groups');

        $box = $W->VBoxItems();
        $page->addItem($box);


        include_once $GLOBALS['babInstallPath']."utilit/grptreeincl.php";


        $tree = new bab_grptree();
        $grps = $tree->getGroups(BAB_REGISTERED_GROUP, '%2$s > ', true);
        $groups = array();
        foreach ($grps as $grp) {
            $groups[$grp['id']] = array(
                'name' => $grp['name'],
                'parent' => $grp['id_parent'],
                'allusers' => 0
            );
        }


        foreach ($groups as $groupId => &$group) {
            $users = bab_getGroupsMembers($groupId);
            if ($users) {
                $group['users'] = count($users);
                $groups[$groupId]['allusers'] += $group['users'];
                $parentId = $group['parent'];
                while (isset($groups[$parentId])) {
                    $groups[$parentId]['allusers'] += $group['users'];
                    $parentId = $groups[$parentId]['parent'];
                }
            } else {
                $group['users'] = 0;
            }

            $users = bab_getGroupsMembers($groupId, false, true);
            if ($users) {
                $group['disabled'] = count($users) - $group['users'];
            } else {
                $group['disabled'] = 0;
            }
        }

        bab_Sort::asort($groups, 'name');
        bab_Sort::asort($groups, 'allusers');

        foreach ($groups as $groupId => $group) {

            $disabledLabel = '';
            if ($group['disabled'] > 0) {
                $disabledLabel = '+' . $group['disabled'];
            }
            $line = $W->FlowItems(
                $W->Label($group['users'] . ' / ' . $group['allusers'])
                    ->setSizePolicy('widget-6em')
                    ->addClass('badge'),
                $W->Label($disabledLabel)
                    ->setSizePolicy('widget-4em'),
//                 $W->Label($groupId)
//                     ->setSizePolicy('widget-4em'),
                $W->Link(
                    $group['name'],
                    '?tg=group&idx=Members&item=' . $groupId
                )
            );

            $line->setSizePolicy('widget-list-element');
            if ($group['allusers'] == 0) {
                $line->addClass('widget-strong');
            }

            $box->addItem($line);
        }

        return $page;
    }
}
